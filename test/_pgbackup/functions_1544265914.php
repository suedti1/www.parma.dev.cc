<?php
if ( ! function_exists( 'fgh_setup' ) ) :

function fgh_setup() {

    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     */
    /* Pinegrow generated Load Text Domain Begin */
    load_theme_textdomain( 'Parma', get_template_directory() . '/languages' );
    /* Pinegrow generated Load Text Domain End */

    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );

    /*
     * Let WordPress manage the document title.
     */
    add_theme_support( 'title-tag' );
    
    /*
     * Enable support for Post Thumbnails on posts and pages.
     */
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 825, 510, true );

    // Add menus.
    register_nav_menus( array(
        'primary' => __( 'Primary Menu', 'fgh' ),
        'social'  => __( 'Social Links Menu', 'fgh' ),
    ) );

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support( 'html5', array(
        'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
    ) );

    /*
     * Enable support for Post Formats.
     */
    add_theme_support( 'post-formats', array(
        'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
    ) );
}
endif; // fgh_setup

add_action( 'after_setup_theme', 'fgh_setup' );


if ( ! function_exists( 'fgh_init' ) ) :

function fgh_init() {

    
    // Use categories and tags with attachments
    register_taxonomy_for_object_type( 'category', 'attachment' );
    register_taxonomy_for_object_type( 'post_tag', 'attachment' );

    /*
     * Register custom post types. You can also move this code to a plugin.
     */
    /* Pinegrow generated Custom Post Types Begin */

    /* Pinegrow generated Custom Post Types End */
    
    /*
     * Register custom taxonomies. You can also move this code to a plugin.
     */
    /* Pinegrow generated Taxonomies Begin */

    /* Pinegrow generated Taxonomies End */

}
endif; // fgh_setup

add_action( 'init', 'fgh_init' );


if ( ! function_exists( 'fgh_widgets_init' ) ) :

function fgh_widgets_init() {

    /*
     * Register widget areas.
     */
    /* Pinegrow generated Register Sidebars Begin */

    /* Pinegrow generated Register Sidebars End */
}
add_action( 'widgets_init', 'fgh_widgets_init' );
endif;// fgh_widgets_init



if ( ! function_exists( 'fgh_customize_register' ) ) :

function fgh_customize_register( $wp_customize ) {
    // Do stuff with $wp_customize, the WP_Customize_Manager object.

    /* Pinegrow generated Customizer Controls Begin */

    /* Pinegrow generated Customizer Controls End */

}
add_action( 'customize_register', 'fgh_customize_register' );
endif;// fgh_customize_register


if ( ! function_exists( 'fgh_enqueue_scripts' ) ) :
    function fgh_enqueue_scripts() {

        /* Pinegrow generated Enqueue Scripts Begin */

    wp_deregister_script( 'modernizr' );
    wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/vendor/modernizr.js', false, null, false);

    wp_deregister_script( 'jquery' );
    wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/vendor/jquery.min.js', false, null, false);

    wp_deregister_script( 'foundation' );
    wp_enqueue_script( 'foundation', get_template_directory_uri() . '/js/foundation.min.js', false, null, false);

    wp_deregister_script( 'jqueryfancybox' );
    wp_enqueue_script( 'jqueryfancybox', 'https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.2/dist/jquery.fancybox.min.js', false, null, false);

    wp_deregister_script( 'foundation' );
    wp_enqueue_script( 'foundation', 'https://cdnjs.cloudflare.com/ajax/libs/foundation/6.2.3/foundation.min.js', false, null, true);

    /* Pinegrow generated Enqueue Scripts End */

        /* Pinegrow generated Enqueue Styles Begin */

    wp_deregister_style( 'foundation' );
    wp_enqueue_style( 'foundation', get_template_directory_uri() . '/css/foundation.css', false, null, 'all');

    wp_deregister_style( 'style' );
    wp_enqueue_style( 'style', get_bloginfo('stylesheet_url'), false, null, 'all');

    wp_deregister_style( 'jqueryfancybox' );
    wp_enqueue_style( 'jqueryfancybox', 'https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.2/dist/jquery.fancybox.min.css', false, null, 'all');

    wp_deregister_style( 'style-1' );
    wp_enqueue_style( 'style-1', get_template_directory_uri() . '/custom.scss', false, null, 'all');

    wp_deregister_style( 'typexstylesheet' );
    wp_enqueue_style( 'typexstylesheet', get_template_directory_uri() . '/typex-stylesheet4.css', false, null, 'all');

    wp_deregister_style( 'fsnavbar' );
    wp_enqueue_style( 'fsnavbar', get_template_directory_uri() . '/components/pg.fs-navbar/css/fs-navbar-1.css', false, null, 'all');

    wp_deregister_style( 'foundation' );
    wp_enqueue_style( 'foundation', 'https://cdnjs.cloudflare.com/ajax/libs/foundation/6.2.3/foundation.min.css', false, null, 'all');

    /* Pinegrow generated Enqueue Styles End */

    }
    add_action( 'wp_enqueue_scripts', 'fgh_enqueue_scripts' );
endif;

function pgwp_sanitize_placeholder($input) { return $input; }
/*
 * Resource files included by Pinegrow.
 */
/* Pinegrow generated Include Resources Begin */

    /* Pinegrow generated Include Resources End */


//CUSTOM CODE
		    wp_deregister_script( 'jquery' );
    wp_enqueue_script( 'jquery', '/wp-includes/js/jquery/jquery.js?ver=1.12.4', false, null, false);

      wp_enqueue_script( 'migrate', '/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1', false, null, false);
?>
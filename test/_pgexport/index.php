<?php
get_header(); ?>

                <div class="row">
                    <div class="large-12 columns">
                        <div class="wrapper">
                            <a data-fancybox data-type="iframe" data-src="/proposta/" href="javascript:;">
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/partecipa.svg" class="float-center above-video btn-partecipa">
                            </a>
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/slogan.svg" class="slogan-home"/>
                            <!--
                    <div class="flex-video" style="width: 1600;">
                        <iframe src="http://player.vimeo.com/video/60122989" width="400" height="225" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                    </div>
                  -->
                            <section id="big-video">
                                <video loop autoplay muted preload="auto" id="bgvid">
                                    <source src="https://parma.easy2use.eu/hd0047.mp4" type="video/mp4">
                                    <?php _e( 'Your browser does not support the video tag.', 'Parma' ); ?>
                                </video>
                            </section>
                            <a data-fancybox data-width="1080" data-height="720" href="http://www.parma2.dev.cc/wp-content/themes/parma/img/hd0047.mp4">
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/vid_play.svg" class="vid-play">
                            </a>
                        </div>
                    </div>
                </div>
                <?php if ( have_posts() ) : ?>
                    <?php while ( have_posts() ) : the_post(); ?>
                        <div class="row type-txt-small-sans text-below">
                            <div class="columns large-2">
</div>
                            <div class="columns large-10">
                                <h4 class="type-txt-small-sans"><?php single_post_title( 'k' ); ?></h4>
                                <div class="row main-txt">
                                    <div class="columns large-4 small-12">
                                        <p><p class="type-txt-big-sans"><?php the_content(); ?></p><p class="type-txt-big-sans"><?php the_content(); ?></p></p>
                                    </div>
                                </div>
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/vid_play.svg"/>
                                <div class="row">
                                    <div class="columns medium-12 large-12">
                                        <p></p>
                                    </div>
                                </div>
                                <div class="row type-txt-small-sans">
                                    <div class="columns large-4 ">
                                        <h4 class="type-txt-small-sans"><?php _e( 'Credits', 'Parma' ); ?></h4>
                                    </div>
                                </div>
                                <div class="row type-txt-small-sans">
                                    <div class="columns large-4">
                                        <p><p class="type-txt-big-sans"><?php _e( 'Mauris tortor purus, lorem pellen tesque non orci quis, porttitor rutrum nulla. Phasellus feugiat quam nec lorem porttitor pretium. Ut quis egestas tellus. Proin euismod arcu tellus, sed fringilla tellus aliquet id. Suspendisse id sapien quis neque commodo fermentum id sit amet elit. Nam nec sollicitudin lectus, vitae scelerisque massa. Fusce aliquam mollis vulputate. Proin at mattis arcu. Suspendisse consequat risus eget ex viverra, nec luctus quam efficitur...dd', 'Parma' ); ?></p></p>
                                    </div>
                                </div>
                                <div class="row type-txt-small-sans">
                                    <div class="columns medium-12 large-12">
                                        <p><?php _e( '.', 'Parma' ); ?></p>
                                        <div class="type-txt-small-sans">
                                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo1.svg" class="logos">
                                        </div>
                                        <div class="type-txt-small-sans">
                                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo_Parma2020_web.svg" class="parma2020 logos">
                                        </div>
                                        <div class="type-txt-small-sans">
                                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/ICOM_NC_Italy2.svg" class="icom logos">
                                        </div>
                                        <div class="type-txt-small-sans">
                                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logoIBC.svg" class="regione logos">
                                        </div>
                                        <div class="type-txt-small-sans">
                                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/comunita_dei_musei.svg" class="comunita logos">
                                        </div>
                                        <div class="type-txt-small-sans">
                                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/btn_musei.svg" class="btn-musei logos">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php else : ?>
                    <p><?php _e( 'Sorry, no posts matched your criteria.', 'Parma' ); ?></p>
                <?php endif; ?>

<?php get_footer(); ?>
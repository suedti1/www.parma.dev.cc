<?php
get_header(); ?>

<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
        <div class="row type-txt-small-sans text-below">
            <div class="columns medium-offset-3 large-9">
                <?php the_content(); ?>
            </div>
        </div>
    <?php endwhile; ?>
<?php else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.', 'Parma' ); ?></p>
<?php endif; ?>

<?php get_footer(); ?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <?php wp_head(); ?>
    </head>
    <body class="<?php echo implode(' ', get_body_class()); ?>">
        <div class="pagewrapper">
            <!-- Header and Nav -->
            <div class="nav-placeholder"></div>
            <header id="Header" class="hides">
                <div class="row">
                    <div class="large-12 columns small-6">
                        <h1></h1>
                        <h1><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/triangle_mob.svg" height="20px" class="triangle-mob"></h1>
                    </div>
                    <div class="medium-4 columns small-6">
                        <div id="fs-navbar-1-1" class="fs-navbar-1-1">
                            <input id="fs-checkbox-trigger" type="checkbox" class="fs-checkbox-trigger">
                            <label for="fs-checkbox-trigger">
                                <span class="fs-navbar-icon right round white effect-slide-in-top"> <span></span> <span></span> <span></span> <span></span> </span>
                            </label>
                            <header class="fs-navbar">
                                <nav>
                                    <ul>
                                        <li>
                                            <a href="#" title="#"><?php _e( 'Menu Item 1', 'Parma' ); ?></a>
                                        </li>
                                        <li>
                                            <a href="#" title="#"><?php _e( 'Menu Item 2', 'Parma' ); ?></a>
                                        </li>
                                        <li>
                                            <a href="#" title="#"><?php _e( 'Menu Item 3', 'Parma' ); ?></a>
                                        </li>
                                        <li>
                                            <a href="#" title="#"><?php _e( 'Duplicate Li for More', 'Parma' ); ?></a>
                                        </li>
                                    </ul>
                                </nav>
                            </header>
                        </div>
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/menu.svg" style="visibility:hidden" class="btn-menu"/>
                    </div>
                </div>
            </header>
            <div class="row wp-site-content">
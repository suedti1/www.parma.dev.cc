
            </div>
        </header>
        <!-- End Header and Nav -->
        <!-- First Band (Image) -->
        <div class="row">
            <div class="large-12 columns">
                <div class="wrapper">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/partecipa.svg" class="float-center above-video">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/slogan.svg" class="slogan-home"/>
                    <div class="flex-video" style="width: 1600;"> 
                        <iframe src="http://player.vimeo.com/video/60122989" width="400" height="225" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>                         
                    </div>
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/vid_play.svg" class="vid-play">
                </div>
            </div>
        </div>
        <!-- Second Band (Image Left with Text) -->
        <div class="row type-txt-small-sans">
            <div class="columns large-2">
</div>
            <div class="columns large-10">
                <h4 class="type-txt-smasll-sans"><?php single_post_title( 'k' ); ?></h4>
                <div class="row main-txt">
                    <div class="columns large-4 small-12">
                        <p><p class="type-txt-big-sans"><?php _e( 'Mauris tortor purus, lorem pellen tesque non orci quis, porttitor rutrum nulla. Phasellus feugiat quam nec lorem porttitor pretium. Ut quis egestas tellus. Proin euismod arcu tellus, sed fringilla tellus aliquet id. Suspendisse id sapien quis neque commodo fermentum id sit amet elit. Nam nec sollicitudin lectus, vitae scelerisque massa. Fusce aliquam mollis vulputate. Proin at mattis arcu. Suspendisse consequat risus eget ex viverra, nec luctus quam efficitur.', 'fgh' ); ?></p></p>
                    </div>
                </div>
                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/vid_play.svg"/>
                <div class="row">
                    <div class="columns medium-12 large-12"> 
                        <p><?php _e( '.', 'fgh' ); ?></p> 
                    </div>
                </div>
                <div class="row type-txt-small-sans">
                    <div class="columns medium-12 large-12"> 
                        <p><?php _e( '.', 'fgh' ); ?></p> 
                    </div>
                    <h4 class="type-txt-small-sans"><?php _e( 'Credits', 'fgh' ); ?></h4>
                    <div class="columns large-4 large-pull-8">
                        <p><p class="type-txt-big-sans"><?php _e( 'Mauris tortor purus, lorem pellen tesque non orci quis, porttitor rutrum nulla. Phasellus feugiat quam nec lorem porttitor pretium. Ut quis egestas tellus. Proin euismod arcu tellus, sed fringilla tellus aliquet id. Suspendisse id sapien quis neque commodo fermentum id sit amet elit. Nam nec sollicitudin lectus, vitae scelerisque massa. Fusce aliquam mollis vulputate. Proin at mattis arcu. Suspendisse consequat risus eget ex viverra, nec luctus quam efficitur.', 'fgh' ); ?></p></p>
                    </div>
                </div>
                <div class="row type-txt-small-sans">
                    <div class="columns medium-12 large-12"> 
                        <p><?php _e( '.', 'fgh' ); ?></p> 
                    </div>
                    <div class="type-txt-small-sans">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo1.svg">
                    </div>
                    <div class="type-txt-small-sans">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo_Parma2020_web.svg">
                    </div>
                    <div class="type-txt-small-sans">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/ICOM_NC_Italy2.svg">
                    </div>
                    <div class="type-txt-small-sans">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logoIBC.svg">
                    </div>
                    <div class="type-txt-small-sans">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/comunita_dei_musei.svg">
                    </div>
                    <div class="type-txt-small-sans">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/btn_musei.svg">
                    </div>
                </div>
            </div>
        </div>
        <!-- Third Band (Image Right with Text) -->
        <!-- Footer -->
        <footer class="row">
            <div class="large-12 columns">
                <hr/>
                <div class="row">
                    <div class="large-6 columns">
                        <p><?php _e( '© Comune di Parma 2018 P.iva. 00000000 Privacy Cookies Imprint Contact', 'fgh' ); ?></p>
                    </div>
                    <div class="large-6 columns">
                        <ul class="float-right menu">
                            <li>
                                <a href="#"><?php _e( 'Link 1', 'fgh' ); ?></a>
                            </li>
                            <li>
                                <a href="#"><?php _e( 'Link 2', 'fgh' ); ?></a>
                            </li>
                            <li>
                                <a href="#"><?php _e( 'Link 3', 'fgh' ); ?></a>
                            </li>
                            <li>
                                <a href="#"><?php _e( 'Link 4', 'fgh' ); ?></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <script>
      $(document).foundation();
    </script>
        <?php wp_footer(); ?>
    </body>
</html>

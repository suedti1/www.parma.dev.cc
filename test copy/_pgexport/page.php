<?php
get_header(); ?>

                <div class="row">
                    <div class="large-12 columns">
                        <div class="wrapper">
                            <a data-fancybox data-type="iframe" data-src="http://www.parma2.dev.cc/hello-world/" href="javascript:;"> </a>
                            <!--
                    <div class="flex-video" style="width: 1600;">
                        <iframe src="http://player.vimeo.com/video/60122989" width="400" height="225" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                    </div>
                  -->
                            <a data-fancybox data-width="1080" data-height="720" href="http://www.parma2.dev.cc/wp-content/themes/parma/img/hd0047.mp4"> </a>
                        </div>
                    </div>
                </div>
                <?php if ( have_posts() ) : ?>
                    <?php while ( have_posts() ) : the_post(); ?>
                        <div class="row type-txt-small-sans text-below">
                            <div class="columns large-2">
</div>
                            <div class="columns large-10">
                                <h4 class="type-txt-small-sans"><?php single_post_title(); ?></h4>
                                <div class="row main-txt">
                                    <div class="columns large-4 small-12">
                                        <p><p class="type-txt-big-sans"><?php the_content( __( 'More', 'Parma' ) ); ?></p></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="columns medium-12 large-12">
                                        <p></p>
                                    </div>
                                </div>
                                <div class="row type-txt-small-sans">
                                    <div class="columns large-4 ">
                                        <h4 class="type-txt-small-sans"><?php _e( 'Credits', 'Parma' ); ?></h4>
                                    </div>
                                </div>
                                <div class="row type-txt-small-sans">
                                    <div class="columns large-4">
                                        <p><p class="type-txt-big-sans"><?php _e( 'Form above this row..ds', 'Parma' ); ?></p></p>
                                    </div>
                                </div>
                                <div class="row type-txt-small-sans">
                                    <div class="columns medium-12 large-12">
                                        <p><?php _e( '.', 'Parma' ); ?></p>
                                        <div class="type-txt-small-sans">
                                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo1.svg" class="logos">
                                        </div>
                                        <div class="type-txt-small-sans">
                                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo_Parma2020_web.svg" class="parma2020 logos">
                                        </div>
                                        <div class="type-txt-small-sans">
                                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/ICOM_NC_Italy2.svg" class="icom logos">
                                        </div>
                                        <div class="type-txt-small-sans">
                                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logoIBC.svg" class="regione logos">
                                        </div>
                                        <div class="type-txt-small-sans">
                                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/comunita_dei_musei.svg" class="comunita logos">
                                        </div>
                                        <div class="type-txt-small-sans">
                                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/btn_musei.svg" class="btn-musei logos">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php else : ?>
                    <p><?php _e( 'Sorry, no posts matched your criteria.', 'Parma' ); ?></p>
                <?php endif; ?>

<?php get_footer(); ?>

            </div>
            <!-- End Header and Nav -->
            <!-- First Band (Image) -->
            <!-- Second Band (Image Left with Text) -->
            <!-- Third Band (Image Right with Text) -->
            <!-- Footer -->
            <div class="row faq-row">
                <div class="columns medium-12 large-12">
                    <div>
                        <div class="faq-cont">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/faq.svg" class="faq">
                        </div>
                    </div>
                </div>
            </div>
            <footer class="row">
                <div class="large-12 columns">
                    <hr/>
                    <div class="row">
                        <div class="large-6 columns">
</div>
                        <div class="large-6 columns">
                            <ul class="float-right menu">
                                <li>
                                    <a href="#"><?php _e( '© Comune di Parma 2018', 'Parma' ); ?></a>
                                </li>
                                <li>
                                    <a href="#"><?php _e( 'P.iva. 00000000', 'Parma' ); ?> </a>
                                </li>
                                <li>
                                    <a href="#"><?php _e( 'Privacy & Cookies', 'Parma' ); ?></a>
                                </li>
                                <li>
                                    <a href="#"><?php _e( 'Contatti & Imprint', 'Parma' ); ?></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <nav class="nav fixed-nav">
                    <div class="row nav-sticky">
                        <div class="large-12 columns small-12">
                            <span class="type-xs-txt-big-sans mob-slogan"> <?php _e( 'Qui prima o poi', 'Parma' ); ?> </span>
                        </div>
                        <div class="medium-4 columns small-4">
                            <!--<img src="img/menu.svg" class="btn-menu"/>-->
                        </div>
                    </div>
                </nav>
                <div class="lowerC">
                    <div class"faq_cont">
                        <div class="row">
                            <div class="large-offset-3">
                                <h2><?php _e( 'FAQ', 'Parma' ); ?></h2>
                            </div>
                        </div>
                        <div class="row faq-txt">
                            <div class="large-4 columns large-offset-2">
                                <h3 class="type-txt-bold-small-sans"> <?php _e( 'Cosa Posso Segnalare?', 'Parma' ); ?> </h3>
                                <p class="type-txt-small-serif"><?php _e( 'Sulla piattaforma digitale', 'Parma' ); ?> <em><?php _e( 'Qui, prima e poi', 'Parma' ); ?></em> <?php _e( 'è possibile segnalare tutto ciò che si ritiene culturalmente importante e che caratterizza e identifica una comunità specifica. La sua importanza non è necessariamente determinata da un valore storico-artistico quanto ad un valore identitario (nel quale ci si riconosce) di una comunità, che questo bene rappresenta. Bene che può essere di natura materiale (a partire da un immobile fino a comprendere oggetti d&rsquo;uso quotidiano o anche giochi) oppure immateriale (leggende, canti tradizionali, riti o ritualità non necessariamente religiose).', 'Parma' ); ?></p>
                                <h3 class="type-txt-bold-small-sans"> <?php _e( 'Cosa si intende con “comunità patrimoniale”?', 'Parma' ); ?></h3> 
                                <p class="type-txt-small-serif"><?php _e( 'Riprendendo la suggestiva immagine della stalla, che arriva dai racconti dei nostri antenati, intesa come luogo in cui gruppi di famiglie di una stessa comunità si ritrovava per condividere momenti di vita, tramandare canti, leggende, saperi; con comunità patrimoniale si intende riprendere e riavviare quelle comunità composte da diverse persone unite da elementi comuni e che, attraverso quegli elementi, possano sentirsi parte di una realtà condivisa.', 'Parma' ); ?> <br/><?php _e( 'In un mondo che porta sempre più il soggetto singolo ad essere solo e costretto all&rsquo;autoreferenziazione, costringendolo a vivere le relazioni coi pari sempre più difficili frammentarie, l&rsquo;appartenenza ad una comunità patrimoniale può riportare la persona a vivere la socialità in maniera positiva, tesa e protesa a esprimere i propri valori identitari nel rispetto dei valori altrui. Vivere l&rsquo;identità in condivisione con altri che, in quella stessa identità ,si rispecchiano; dando così avvio a processi di confronto e dialogo con le diverse identità che persistono su quello stesso territorio e vivere in modo civile, positivo e proattivo il sistema democratico nel quale ogni cittadino si rispecchia.', 'Parma' ); ?></p>
                            </div>
                            <div class="large-4 columns  end">
                                <h3 class="type-txt-bold-small-sans"> <?php _e( 'Che cos&rsquo;è un&rsquo;eredità culturale?', 'Parma' ); ?> </h3>
                                <p class="type-txt-small-serif"><?php _e( 'Con eredità culturale si intente indicare un insieme di beni, oggetti, tradizioni, melodie, paesaggi, percorsi che fanno parte del nostro vivere la realtà che vi circonda e che vi porta ad elaborare un sentimento di condivisione con gli altri membri della comunità di appartenenza o di una comunità a cui vi sentite vicini ed intimamente legati.', 'Parma' ); ?></p>
                                <h3 class="type-txt-bold-small-sans"> <?php _e( 'Perché partecipare?', 'Parma' ); ?> </h3>
                                <p class="type-txt-small-serif"><?php _e( 'Partecipare per dare respiro e vita a ciò che sentite intimamente vostro, per trasmettere la vostra identità, i vostri saperi, i valori entro i quali voi vi rispecchiate come individui all&rsquo;interno di una realtà territoriale concreta. Una realtà composta da tanti individui che, non sapendolo, potrebbero condividere esperienze, tradizioni, oggetti, opere d&rsquo;arte o edifici e tanto altro ancora sia ritenuto importante culturalmente e che si intenda preservare e trasmettere alle generazioni future.', 'Parma' ); ?></p>
                            </div>                             
                        </div>
                    </div>
                </div>
            </footer>
            <script>
/*
        var vid = document.getElementById("bgvid");
        var pauseButton = document.querySelector("#");

        function vidFade() {
          vid.classList.add("stopfade");
        }

        vid.addEventListener('ended', function()
        {
        // only functional if "loop" is removed
        vid.pause();
        // to capture IE10
        vidFade();
        });


        pauseButton.addEventListener("click", function() {
          vid.classList.toggle("stopfade");
          if (vid.paused) {
            vid.play();
            pauseButton.innerHTML = "Pause";
          } else {
            vid.pause();
            pauseButton.innerHTML = "Paused";
          }
        });


$(document).on("scroll", function() {
  if ($(document).scrollTop() > 50) {
    $(".pagewrapper").removeClass("scrolltop").addClass("scrolldown");
    $(".nav-placeholder").height($(".nav").outerHeight());
    console.log($(".nav").outerHeight());
  } else {
    $(".pagewrapper").removeClass("scrolldown").addClass("scrolltop");
    $(".nav-placeholder").height(0);
  }
});
*/

$(window).scroll(function() {
if ($(this).scrollTop() > 1){$('nav').addClass("sticky");}
else{$('nav').removeClass("sticky");}
});

                //faq menu
		jQuery(".faq").click(function(event) {
			  event.preventDefault();

    if (jQuery('#lowerC').css('display') == 'block') {
        var height = '-=' + jQuery('.lowerC').height();
    } else {
        var height = '+=' + jQuery('.lowerC').height();
    }
    //jQuery("#panel").slideToggle(2000);


// $("#panel").toggleClass("panelUP",2000);
 $(".lowerC").toggleClass("lowerUP",2000);


   // jQuery("#upper").animate({
  //      bottom: height
  //  }, 4000)
});




        </script>
            <script>
      $(document).foundation();
    </script>
        </div>
        <!--pagewrapper -->
        <?php wp_footer(); ?>
    </body>
</html>
